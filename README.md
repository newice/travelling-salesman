## Introduction

Die L�sung ist eine Konsolenapplikation in DotNet Core.
Der Output ist eine KML Datei (result.kml) die man sich in Google Mpas bzw. Google Earth anzeigen lassen kann.
Erstes Argument ist der relative Pfad zur CSV Datei, Zweites Argument ist die Anzahl der zu �berpr�fenden n�chsten Nachbarn(Default 2) 

Die entstandene kml Datei (https://gitlab.com/newice/travelling-salesman/-/blob/master/Assets/result.kml) kann z.b. hier visualisiert werden: http://kmlviewer.nsspot.net/
# Build

```
dotnet restore
dotnet build
```

alternatively use VisualStudio