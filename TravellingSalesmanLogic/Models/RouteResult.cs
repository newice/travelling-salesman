﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravellingSalesmanLogic.Models
{
    public class RouteResult
    {
        public IEnumerable<int> points { get; set; }
        public double TotalDistance { get; set; }
    }
}
