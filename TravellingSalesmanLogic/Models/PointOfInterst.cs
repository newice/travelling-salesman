﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace TravellingSalesmanLogic.Models
{
    public class PointOfInterst
    {
        [Name("Nummer")]
        public int number { get; set; }

        [Name("msg Standort")]
        public string name { get; set; }

        [Name("Straße")]
        public string street { get; set; }

        [Name("Hausnummer")]
        public string street_nr { get; set; }

        [Name("PLZ")]
        public string zip { get; set; }

        [Name("Ort")]
        public string city { get; set; }

        [Name("Breitengrad")]
        public double lat { get; set; }

        [Name("Längengrad")]
        public double lon { get; set; }
    }
}
