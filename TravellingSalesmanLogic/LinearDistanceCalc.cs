﻿using System;
using System.Collections.Generic;
using System.Text;
using TravellingSalesmanLogic.Models;

namespace TravellingSalesmanLogic
{
    public class LinearDistanceCalc : IDistanceProvider
    {
        /// <summary>
        /// 
        /// https://en.wikipedia.org/wiki/Haversine_formula
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public double CalculateDistance(PointOfInterst p1, PointOfInterst p2)
        {
            double R = 6371; // Radius of the earth in km
            var dLat = ConvertDegreesToRadians(p2.lat - p1.lat);
            var dLon = ConvertDegreesToRadians(p2.lon - p1.lon);
            var a =
              Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
              Math.Cos(ConvertDegreesToRadians(p1.lat)) * Math.Cos(ConvertDegreesToRadians(p2.lat)) *
              Math.Sin(dLon / 2) * Math.Sin(dLon / 2);

            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c; // Distance in km
            return d;
        }

        public double ConvertRadiansToDegrees(double radians)
        {
            double degrees = (180 / Math.PI) * radians;
            return (degrees);
        }

        public double ConvertDegreesToRadians(double degrees)
        {
            double radians = degrees * (Math.PI / 180);
            return (radians);
        }

    }
}
