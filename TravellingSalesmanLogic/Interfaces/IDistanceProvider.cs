﻿using System;
using System.Collections.Generic;
using System.Text;
using TravellingSalesmanLogic.Models;

namespace TravellingSalesmanLogic
{
    public interface IDistanceProvider
    {
        public double CalculateDistance(PointOfInterst p1, PointOfInterst p2);
    }
}
