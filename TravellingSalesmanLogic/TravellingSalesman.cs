﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TravellingSalesmanLogic.Models;

namespace TravellingSalesmanLogic
{
    public class TravellingSalesman
    {
        private IEnumerable<PointOfInterst> _records;
        private IDistanceProvider _calculator;
        private Dictionary<int, Dictionary<int, double>> _distanceMatrix;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="records"></param>
        /// <param name="calculator"></param>
        public TravellingSalesman(IEnumerable<PointOfInterst> records, IDistanceProvider calculator = null)
        {
            _records = records;
            _calculator = calculator ?? new LinearDistanceCalc();
            _distanceMatrix = records
                .Select(i => new KeyValuePair<int, Dictionary<int, double>>(i.number, GetDistanceVector(i)))
                .ToDictionary(x => x.Key, x => x.Value);
        }

        private Dictionary<int, double> GetDistanceVector(PointOfInterst item)
        {
           return this._records
                .Select(i => new KeyValuePair<int, double>(i.number, GetDistance(item, i)))
                .ToDictionary(x => x.Key, x => x.Value);
        }

        public RouteResult FindShortestTrip(PointOfInterst p, IEnumerable<int> exclude = null, double previousDistance = 0, int depth = 1, PointOfInterst prev = null)
        {
            if (prev == null)
            {
                prev = p;
            }
            if(exclude == null)
            {
                exclude = new List<int>();
            }

            exclude = exclude.Union(new List<int>() { p.number });

            var v = _distanceMatrix[p.number].AsEnumerable().Where(x => !exclude.Contains( x.Key)).OrderBy(x => x.Value).Take(depth).ToList();
                RouteResult res = null; 
            if (v.Count() > 0)
            {
                res = v.Select(item => FindShortestTrip(_records.FirstOrDefault(x => x.number == item.Key), exclude, previousDistance + item.Value, depth,p))
                    .OrderBy(x => x.TotalDistance)
                    .First();
            }
            else
            {
                res = new RouteResult
                {
                    TotalDistance = previousDistance + GetDistance(p, _records.FirstOrDefault(x => x.number == exclude.First())),
                    points = exclude
                };
            }

            return res;

        }

        private double GetDistance(PointOfInterst from, PointOfInterst to)
        {
            return _calculator.CalculateDistance(from, to); 
        }
    }
}
