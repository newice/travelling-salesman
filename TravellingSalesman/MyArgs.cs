﻿using PowerArgs;
using System;
using System.Collections.Generic;
using System.Text;

namespace TravelingSalesman
{
    public class MyArgs
    {
        [ArgRequired(PromptIfMissing = true), ArgPosition(0)]
        public string Path { get; set; }

        [ArgRange(1, 5), ArgDefaultValue(2), ArgPosition(1)]
        public int Nearest { get; set; }
    }
}
