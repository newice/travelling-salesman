﻿using CsvHelper;
using PowerArgs;
using SharpKml.Base;
using SharpKml.Dom;
using SharpKml.Engine;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using TravellingSalesmanLogic;
using TravellingSalesmanLogic.Models;
using TimeSpan = System.TimeSpan;

namespace TravelingSalesman
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Welcome Travelling Salesman!");
            var parsedArgs = Args.Parse<MyArgs>(args);
            var path = parsedArgs.Path;
            var nearest = parsedArgs.Nearest;
            const int extimatedCallsPerSecond = 25000;

            Console.Write($"Reading file: {path} ...");

            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                IEnumerable<PointOfInterst> points = csv.GetRecords<PointOfInterst>().ToList();
                var pointsCount = points.Count();
                Console.WriteLine($"read {pointsCount} points.");
                TravellingSalesman t = new TravellingSalesman(points);
                Console.WriteLine($"This program uses the Nearest Naighbour Algorithm with checking each {nearest} nearest neighbours.");
                var calculatedFunctionCalls = Math.Pow(nearest, pointsCount -1) + Math.Pow(nearest, pointsCount - 2) -1;
                Console.WriteLine($"Calculeated Function Calls: {calculatedFunctionCalls:0,0}");
                var seconds = calculatedFunctionCalls / extimatedCallsPerSecond;
                TimeSpan ts = TimeSpan.FromSeconds(seconds);
                Console.WriteLine($"Estimated duration with {extimatedCallsPerSecond:0,0} calls/sec: {ts.ToString("g")}");
                Console.Write("Proceed (y/n)? ");
                var key = Console.ReadKey();
                Console.WriteLine();
                if (key.KeyChar.ToString().Equals("y"))
                {
                    var timer = DateTime.Now;
                    var result = t.FindShortestTrip(points.First(), null, 0, nearest);
                    var dtimer = DateTime.Now - timer;
                    Console.WriteLine($"Shortest route with nearest {nearest}: {result.TotalDistance:0.##}km");
                    Console.WriteLine($"Ran {dtimer.TotalSeconds} seconds Function calls: {(calculatedFunctionCalls/dtimer.TotalSeconds):0.##} calls/second");
                    foreach (var item in result.points)
                    {
                        Console.WriteLine($"{item}- {points.FirstOrDefault(x => x.number == item).name}");
                    }
                    Console.WriteLine($"The output will be saved to result.kml");
                    CreateKmlOutput(result, points);
                }

                Console.WriteLine("Bye!");
                Console.ReadKey();
            }
        }

        private static void CreateKmlOutput(RouteResult res, IEnumerable<PointOfInterst> records)
        {
            var points = res.points.ToList();
            points.Add(res.points.First()); // Close the circle;
            var LineString = new LineString
            {
                AltitudeMode = AltitudeMode.Absolute,
                Extrude = true,
                Coordinates = new CoordinateCollection(points.Select(p=>new Vector 
                {
                    Latitude =  records.First(r => r.number == p).lat, 
                    Longitude = records.First(r => r.number == p).lon
                }))
            };
            var route = new Placemark
            {
                Geometry = LineString,
                Name = "Route",
                
            };
            var doc = new Document();
            doc.AddFeature(route);

            foreach(var record in records)
            {
                doc.AddFeature(new Placemark
                {
                    Geometry = new Point
                    {
                        Coordinate = new Vector
                        {
                            Latitude = record.lat,
                            Longitude = record.lon
                        }
                    },
                    Name = record.name,
                    Description = new Description
                    {
                        Text = $"{record.street} {record.street_nr}, {record.zip} {record.city}"
                    }
                });
            }

            KmlFile kml = KmlFile.Create(doc, false);
            using (FileStream stream = File.OpenWrite("result.kml"))
            {
                kml.Save(stream);
            }


        }



    }
}
